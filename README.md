# 3C8 2023
 
 # File Naming Structure

 Project-Component-MemberInitials-Date-V(Week)-iteration

 Example:
 
 Dice-Decoder-MJVF-2701-V1_1 (Week#1_Iteration#1)

-  MJVF: Michael
-  DON: Daire
-  RCF: Rosa
-  LM: Leen
-  WG: Wojtek


# Vegas Dice

# Week #1:
As a group we read through the task brief and established the key requirements that needed to be met to fully comply with the brief limitations.

#Requirements:

•	The Dice must be designed on multisim before being physically built.
•	The physical build should be as efficient as possible requiring the least amount of hardware to be fully functioning.
•	The build must incorporate the 4096-counter integrated chip in its core design.
After identifying the key aspects of the task, each member of the group tried coming up with an individual solution that we shared amongst ourselves to decide the approach we all agreed on.

# Our Approach:

The design we decided to implement would primarily focus on the Counters clock and carry-in relationship, where the carry-in pin turns high whenever the clock would reach the tenth number in the sequence. We wanted to track the number of carry-in impulses created by the clock to use as a reference for the dice to express a given value. The carry-in impulses would be counted from 1-6 and would keep looping around at a very high speed that would be determined by the frequency that the clock is running on. A button press would interrupt the loop and then current number of impulses would need to be converted into a binary number which would then be displayed on the LED’s provided.

Daire:
Designed the looping function which keeps the number of carry-in impulses between 1-6. The loop was created using the 4029 BMS counter and a singular AND gate which was connected to ….

Rosa:
Researched methods of saving the carry-in impulses so that they can be converted into binary values using a custom decoder. The most suitable method of storing the impulses turned out to be with the use of D-Latches. A Quad D-type Flip Flop (40175) was found to be a great choice for our design as the design required 3 D-latches which could all be facilitated by this single chip.


Michael:
Designed a logic gate decoder on multisim for turning the number of carry-in impulses into a binary number. The decoder read in the input values from the D-latches having four different outputs due to the LED’s required needing 4 separate input to display the entire range of numbers on a Vegas style dice. It was constructed with the use of 3 OR gates , 2 AND gates and a single Inverter.

Wojciech:
Researched the required hardware for the design, the IC’s required for the physical implementation are:
4071 (OR Gate) , 4081 (AND Gate) , 4007 (Inverter) , 40175 (Quad D-type Flip Flop), 4029 (BMS Counter)

# Week #2:
